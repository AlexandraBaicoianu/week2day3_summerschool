#include <iostream>
struct Employee
{
	short id;
	int age;
	float wage;
};

struct Company
{
	Employee CEO; // Employee is a struct within the Company struct
	int numberOfEmployees;
};

int main()
{
	Company myCompany = { { 1, 42, 60000.0f }, 5 };
	std::cout << myCompany.CEO.wage << std::endl;
}