#include <iostream>
struct A
{
	short x;
	A() = default; //C++11 - part instructs the compiler to generate the default implementation for the function
	virtual ~A() = default; //C++11
};

int main()
{
	A something;
	std::cout << something.x << std::endl;
}

