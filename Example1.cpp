#include <iostream>
struct Employee
{
	short id;
	int age;
	double wage;
};

int main()
{
	Employee joe = { 1, 32, 60000.0 };
	Employee frank = { 2, 28 }; 
	std::cout << joe.age << std::endl;
	/*
	C++11
	Employee joe { 1, 32, 60000.0 }; 
	Employee frank { 2, 28 }; 
	*/
}